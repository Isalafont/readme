# Hi there, I'm Isabelle 👋

#### A ✨ Freelance FullStack web developper Ruby On Rails ✨ from Paris, France !

##### Get in Touch:

  [<img src="https://user-images.githubusercontent.com/43042737/158415170-f311d94f-c857-4950-a3fe-d8845994f883.png" width="55" height="55">](https://twitter.com/isalafont)
  [<img src="https://user-images.githubusercontent.com/43042737/158415164-817556f7-20fa-45b6-85ee-9c1a903b1220.png" width="50" height="50">](https://www.linkedin.com/in/isabelle-lafont/)

---

### A little bit more about me

- 👩‍💻 I’m currently learning VueJs, DevOps and Software Architecture
- 👯 I’m looking to collaborate on Open Source Projects !
- 🌍 I'm mostly active within the Ruby on Rails Community *[Woman on Rails][1]* and *[Wnb.rb][2]*
- 💪 Sometimes, I give some talks !
- 📌 I used to be an assistant camera and camera operator in film and adverstising production
- 🎉 In my free time, I enjoy hiking, biking, snowboarding and spending time with my family !
- 🌈 Fun fact: I'm a knitter and crocheter and sometimes, I design Knit accessories ! 🧶

---

### Technologies and Tools

[![My Skills](https://skillicons.dev/icons?i=ruby,rails,js,vue,html,css,sass,bootstrap,tailwind,postgres,redis,git,docker,vscode,wordpress&theme=light)](https://skillicons.dev)


<!-- links to your social media accounts -->
[1]: https://women-on-rails.github.io/ressources/
[2]: https://www.wnb-rb.dev/

---
